<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Condição SWITCH PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
	<style>
		.btn{
			color: #ffffff;
			background-color: #273747;
			padding: 4px 8px;
			font-size: 12pt;
			box-shadow: inset 0 1px 1px #fff, 0 2px 3px #666;
		}
	</style>
</head>
<body>
<div>
	<?php
		$op = $_POST["nSelec"];
		$n = $_POST["nNum"];
		echo "Numero recebido: $n";
			switch ($op){
				case 1:
					$res = pow($n, 2);
					echo "</br>Resultado: $res";
					break;
				case 2:
					$res = pow($n, 3);
					echo "</br>Resultado: $res";
					break;
				case 3:
					$res = sqrt($n);
					echo "</br>Resultado: $res";
					break;
				default:
					echo "</br>ERRO!";
					echo "</br>Escolha uma operação";
			}
	?>
	<form method="post" action="Escolha-operacao.html">
	<input type="submit" name="nVoltar" class="btn" value="Voltar"/>
	</form>
</div>
</body>
</html>