	<?php
	// Função que não retorna valor, apenas mostra na tela //
		function soma($a,$b){
			$res = $a+$b;
			echo "Resultado da Soma: $res</br>";
		}
		// Função que retorna valor, atribuindo este valor à outra variável e depois mostrando na tela // 
		function soma1($a,$b){
			return $a+$b;
		}
		// Função com multiplos parametros, utilizando FOR // 
		function soma2(){
			$v = func_get_args();
			$tot = func_num_args();
			$soma = 0;
			for ($i=0;$i<$tot;$i++){
				$soma = $soma + $v[$i];
			}
			return $soma;
		}
		// Passagem de parametro por Valor -> Apenas o valor da variavel é passado para a função // 
		function teste($x){
			$x += 2;
			echo $x;
		}
		// Passagem de parametro por Referencia -> A variavel em si é passada para a função //  
		function teste2(&$y){
			$y += 3;
			echo $y;
		}
		function ola(){
			echo "Ola, Mundo";
		}
		function mostraValor($v){
			echo "Valor recebido: $v ";
		}
		?>
		
		
		