<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Repetição For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
<div>	
	<?php
	// Passagem de parametro por Valor -> Apenas o valor da variavel é passado para a função // 
		function teste($x){
			$x += 2;
			echo $x;
		}
		$a = 3;
		teste ($a);
		echo "</br> $a";
		echo "</br>-----------------</br>";
	// Passagem de parametro por Referencia -> A variavel em si é passada para a função //  
		function teste2(&$y){
			$y += 3;
			echo $y;
		}
		$b = 5;
		teste2 ($b);
		echo "</br> $b";
	?>
	</div>
</body>
</html>
