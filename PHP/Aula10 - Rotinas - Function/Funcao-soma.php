<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Repetição For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
<div>	
	<?php
	// Função que não retorna valor, apenas mostra na tela //
		function soma($a,$b){
			$res = $a+$b;
			echo "Resultado da Soma: $res</br>";
		}
		soma(15,85);
		soma(12,98);
		$x = 10*9;
		$y = 8*7;
		$resul = soma($x,$y);
		echo "res = $resul";
		echo "</br>--------------------------------</br>";
		// Função que retorna valor, atribuindo este valor à outra variável e depois mostrando na tela // 
		function soma1($a,$b){
			return $a+$b;
		}
		$soma1 = soma1(10,8);
		echo "Resultado da Soma: $soma1</br>";
		$w = 125+5;
		$z = 22+8;
		$soma2 = soma1($w,$z);
		echo "Resultado da Soma: $soma2";
		echo "</br>--------------------------------</br>";
		// Função com multiplos parametros, utilizando FOR // 
		function soma2(){
			$v = func_get_args();
			$tot = func_num_args();
			$soma = 0;
			for ($i=0;$i<$tot;$i++){
				$soma = $soma + $v[$i];
			}
			return $soma;
		}
		$soma3 = soma2(5,10,15,20,25,30,35,40);
		echo "Resultado da Soma: $soma3</br>";
		
?>
	</div>
</body>
</html>












