<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Operadores de Atribuição PHP</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
	<style>
			h2{
			font-size: 14pt;
			color: blue;
			font-weight: bold;
			margin-bottom: -15px;
			margin-top: 0px;
			text-shadow: 1px 1px black;
		}
	</style>
</head>
<body>
<div>
	<?php
	/* Operadores de Atribuição */
		$preco = $_GET["p"];
		echo "<h2> Preço Recebido: R$ ".number_format($preco,2,",",".")."</h2>";
		echo "</br>---------- Exercicio ----------";
		$preco_d = $preco * 0.1;
		$preco_cd = $preco - $preco_d;
		echo "</br> Preço com desconto: R$ ".number_format($preco_cd,2,",",".");
		echo "</br> Valor do desconto: R$ ".number_format($preco_d,2,",",".");
		echo "</br>--------------------------------";
	// Operadores de Incremento e Decremento
		$ano = $_GET["a"];
		echo "</br> Ano Atual: $ano; Ano Anterior: ".--$ano;
	// Variaveis por Referencia
		echo "</br>--------------------------------";
		$a = 5;
		$b = &$a;
		$b += 7;
		$c = $b;
		$c += 10;
		$d = &$c;
		$d += 15;
		echo "</br> Valor de A: $a; Valor de B: $b; Valor de C: $c; Valor de D: $d";
	// Variaveis Variantes
		echo "</br>--------------------------------";
		$site = "Cursoemvideo";
		$$site = "CursoPHP";
		$$$site = "CursoHTML";
		echo "</br> $site; ".$Cursoemvideo."; ".$CursoPHP;
		
		
		
	?>
</div>
</body>
</html>