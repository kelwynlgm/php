<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Operadores de Atribuição PHP</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
	<style>
			h2{
			font-size: 14pt;
			color: blue;
			font-weight: bold;
			margin-bottom: -15px;
			margin-top: 0px;
			text-shadow: 1px 1px black;
		}
	</style>
</head>
<body>
<div>
	<?php
	// Operadores de Atribuição 
		$n1 = 10;
		$n2 = 5;
		echo "<h2> Valores Recebidos $n1 e $n2</h2>";
		echo "</br>---------- Atribuição ----------";
		echo '</br>$n1=$n1+$n2 -> $n1 = '.$n1+=$n2;
		echo '</br>$n1=$n1-$n2 -> $n1 = '.$n1-=$n2;
		echo '</br>$n1=$n1*$n2 -> $n1 = '.$n1*=$n2;
		echo '</br>$n1=$n1/$n2 -> $n1 = '.$n1/=$n2;
		echo '</br>$n1=$n1%$n2 -> $n1 = '.$n1%=$n2;
		echo '</br>$n1=$n1+100 -> $n1 = '.$n1+=100;
		
	?>
</div>
</body>
</html>