<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Repetição DO-WHILE PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
<div>
	<?php
		$v1 = isset($_POST["nV1"])?$_POST["nV1"]:1;
		$v2 = $v1;
		$fat = 1;
		do {
			echo "$fat x $v2 = ";
			$fat *= $v2;
			echo $fat;
			echo "</br>";
			$v2--;
		}while($v2>=1);
		echo "---------------</br>";
		echo "$v1! = ".$fat;
	?>
	</div>
</body>
</html>