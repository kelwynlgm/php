<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Vetores For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
	<div>
	<pre>
	<?php
	// Matrizes são apenas vetores dentro de vetores
		$m = array (1=>array(2=>5,3=>6),
					array(8,8),
					array(9,10));
		$m[2][1] = 1;
		$m[1][3] = $m[2][1];
		print_r($m);
		$c = count($m);
		echo "A matriz tem $c linhas.";
		$t = str_repeat("-",25);
		echo "</br>$t</br>";
	?>
	</pre>
	</div>
</body>