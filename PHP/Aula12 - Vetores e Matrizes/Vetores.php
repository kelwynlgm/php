<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Vetores For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
	<div>	
	<pre>
		<table style="padding:2px" border="1">
	<?php
	// Metodo ARRAY
		$v = array(5,6,10,15,22);
		$v[] = 35;
		print_r ($v);
		$t = str_repeat("-",25);
		echo "$t</br>";
	// Metodo RANGE
		$c = range(0,1,0.1);
		print_r ($c);
		echo "$t</br>";
	// Metodo FOREACH	
		foreach ($c as $valor){
		echo "<td style='padding:2px'>$valor </td>";
		}
		?>
		</table>
	<?php
	// Metodo de vetores com indices de numero personalizados
		$v2 = array (1 => "J", 5 => 20, 10 => 30);
		$v2[15] = "K";
		print_r ($v2);
	// UNSET para excluir elementos do vetor;
		unset($v2[10]);
		print_r ($v2);
		echo "$t</br>";
	// vetores com indices personalizados em strings
		$v3 = array ("Nome" => "Kelwyn",
					 "Idade" => 22,
					 "Altura" => 1.70,
					 "Estudante" => "Sim");
		$v4 = array ("Nome" => "Will",
					 "Idade" => 15,
					 "Altura" => 1.73,
					 "Estudante" => "Sim");
		print_r ($v3);
		print_r ($v4);
		?>
		<table border="1">
		<?php
		foreach($v3 as $ind => $valor2){
			echo "<tr><td style='padding:2px'>$ind </td><td style='padding:2px'> $valor2 </td></tr></br>";
		}
		
	?>
		</table>
	</pre>
	</div>
</body>
</html>