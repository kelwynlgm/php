<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Vetores For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
	<div>	
	<pre>
	<?php
		$v = array(1,2,3,4,5);
		print_r($v);
	// Função 'array_push' para adicionar um elemento ao ultimo indice do vetor / 'array_pop' para eliminar o elemento do ultimo indice do vetor
		array_push($v,6);
		array_pop($v);
		print_r($v);
		$t = str_repeat("-",25);
		echo "</br>$t</br>";
	// Funçãp 'array_unshift' para adicionar um elemento ao primeiro indice do vetor / 'array_shift' para eliminar o elemento od primeiro indice do vetor
		$v2 = array(10,20,30,40,50);
		print_r($v2);
		array_unshift($v2,0);
		print_r($v2);
		array_shift($v2);
		print_r($v2);
		echo "</br>$t</br>";
	// Função 'sort' para colocar em ordem crescente, os valores do vetor / 'rsort' para colocar em ordem decrescente / 'asort' para colocar os valores do vetor em order crescente sem alterar o numero dos indices, que permancerão os numeros originais de cada elemento / 'arsort' mesma coisa do 'asort' mas colocando em ordem decrescente
		$v3 = array(7,2,9,5,4);
		arsort($v3);
		print_r($v3);
		echo "</br>$t</br>";
	// Função 'ksort' para colocar o numero dos indices do vetor em ordem crescente / 'krsort' mesma coisa mas só que em ordem decrescente
		$v4 = array(3=>2,1=>4,6=>6,0=>8,5=>10);
		print_r($v4);
		krsort($v4);
		print_r($v4);
	
	?>
	</pre>
	</div>
</body>
</html>