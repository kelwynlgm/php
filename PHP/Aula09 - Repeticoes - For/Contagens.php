<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Repetição FOR PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
<div>
	<?php		
		for($c=1;$c<=10;$c++){
			echo "$c ";
		}
		echo "</br>-----------------</br>";
		for($v=10;$v>=1;$v--){
			echo "$v ";
		}
		echo "</br>-----------------</br>";
		for($v1=0;$v1<=100;$v1+=5){
			echo "$v1 ";
		}
		echo "</br>-----------------</br>";
		for($v2=100;$v2>=0;$v2-=10){
			echo "$v2 ";
		}
		?>
	</div>
</body>
</html>