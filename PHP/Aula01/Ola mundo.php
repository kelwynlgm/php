<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>Testando PHP</title>
	<style>
		body{
			font-family: Arial;
			color: blue;
		}
		
		h1{	
			font-size: 18pt;
			text-shadow: 1px 1px 1px black;
		}
		
		h2{	
			font-size: 14pt;
			text-shadow: 1px 1px 1px black;
			margin-bottom: -5px;
		}
		
		p{
			font-size: 12pt;
			text-shadow: 1px 1px 1px black;
			margin-top: 0px;
		}
	</style>
</head>
<body>
	<h1>Primeiro Programa PHP</h1>
	<?php
		echo "<h2>O l á, M u n d o</h2></br>
		<p>Hehe :p </p>";
	?>
</body>
</html>