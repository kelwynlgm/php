<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Operadores Relacionais PHP</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
	<style>
			h2{
			font-size: 14pt;
			color: blue;
			font-weight: bold;
			margin-bottom: -15px;
			margin-top: 0px;
			text-shadow: 1px 1px black;
		}
	</style>
</head>
<body>
<div>
	<?php
	// Operadores de Comparação e Operador Ternário 
			$a = $_GET["a"];
			$b = $_GET["b"];
			$op = $_GET["op"];
			$soma = $op == "s" ? $a+$b : $a*$b;
			echo $soma;
	// Operador de Identico
			echo "</br>--------------------------------";
			$c = 3;
			$d = "3";
			$r = $c === $d ? "SIM" : "NÃO";
			echo "</br> São identicos? $r";
	// Teste de situação com Operador Ternário	
			echo "</br>--------------------------------";
			$n1 = $_GET["n1"];
			$n2 = $_GET["n2"];
			$media = ($n1+$n2)/2;
			echo "</br> Média: $media; Aluno: ".($media>=7?"APROVADO":"REPROVADO");
	// Teste com operadores lógicos
			echo "</br>--------------------------------";
			$aa = $_GET["aa"];
			$an = $_GET["an"];
			$idade = $aa - $an;
			echo "</br> Idade do Eleitor: $idade; Então o voto é: ".(($idade>=18 && $idade<=65) ? "OBRIGATORIO" : "OPCIONAL");
	?>
</div>
</body>
</html>