<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Funções Aritméticas PHP</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
	<style>
		h2{
			font-size: 16pt;
			color: blue;
			font-weight: bold;
			margin-bottom: -15px;
			margin-top: 0px;
			text-shadow: 1px 1px black;
		}
	</style>
</head>
<body>
<div>
	<?php
	// Funções Aritméticas
		$n = $_GET["n"];
		$n2 = $_GET["n2"];
		$m = ($n+$n2)/2;
		echo "<h2>Valores recebidos $n e $n2</h2>";
		echo "</br>---------- Funções ----------";
		echo "</br> Valor absoluto de $n = ".abs($n);
		echo "</br> Potenciação $n<sup>$n2</sup> = ".pow($n, $n2);	
		echo "</br> Raiz quadrada de $n = ".sqrt($n);
		echo "</br> Arredondamento de $n = ".round($n);
		echo "</br> Valor inteiro de $n = ".intval($n);
		echo "</br> Valor de $n em moeda = ".number_format($n,2,",",".");
	?>
</div>
</body>
</html>