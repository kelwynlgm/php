<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Operadores Aritméticos PHP</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
	<style>
		h2{
			font-size: 16pt;
			color: blue;
			font-weight: bold;
			margin-bottom: -15px;
			margin-top: 0px;
			text-shadow: 1px 1px black;
		}
	</style>
</head>
<body>
<div>
	<?php
		$n = $_GET["n"];
		$n2 = $_GET["n2"];
		$m = ($n+$n2)/2;
		echo "<h2>Valores recebidos $n e $n2</h2>";
		echo "</br>---------- Operações ----------";
		echo "</br>$n+$n2 = ".($n+$n2);
		echo "</br>$n-$n2 = ".($n-$n2);
		echo "</br>$n*$n2 = ".($n*$n2);
		echo "</br>$n/$n2 = ".($n/$n2);
		echo "</br>Módulo de $n/$n2 = ".($n%$n2);
		echo "</br>Média = $m";
		echo "</br>";

?>
	
</div>
</body>
</html>