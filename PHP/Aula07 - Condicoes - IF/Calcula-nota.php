<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Condição SE PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>
<style>
		a {
			color: #ffffff;
			background-color: #273747;
			padding: 7px 11px;
			font-size: 12pt;
			box-shadow: inset 0 1px 1px #fff, 0 2px 3px #666;
		}
		.ap{
			color: blue;
			font-size: 15pt;
			font-weight: bold;
			text-shadow: 1px 1px 1px black;
		}
		.rc{
			color: yellow;
			font-size: 15pt;
			font-weight: bold;
			text-shadow: 1px 1px 1px black;
		}
		.rp{
			color: red;
			font-size: 15pt;
			font-weight: bold;
			text-shadow: 1px 1px 1px black;
		}
		.Calc{
			color: #ffffff;
			background-color: #273747;
			padding: 7px 11px;
			font-size: 12pt;
			box-shadow: inset 0 1px 1px #fff, 0 2px 3px #666;
		}
	</style>
</head>
<body>
<div>
	<?php
		$n1 = $_POST["nN1"];
		$n2 = $_POST["nN2"];
		$media = ($n1+$n2)/2;
			if ($media>=7){
				echo "Media: $media.";
				echo "</br>Aluno <span class='ap'> APROVADO!</span>";
			}
			elseif ($media<7 and $media>=3){
				echo "Media: $media.";
				echo "</br>Aluno em <span class='rc'> RECUPERAÇÃO.</span>";
			}
			elseif ($media<3){
				echo "Media: $media.";
				echo "</br>Aluno <span class='rp'> REPROVADO.</span>";
			}
	?>
	</br>
	</br>
	<form method="post" action="Calcula-nota.html">
	<input type="submit" name="btn" class="Calc" value="Voltar"/>
	</form>
</div>
</body>
</html>