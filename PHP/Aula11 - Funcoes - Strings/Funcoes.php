<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Repetição For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
<div>	
		<?php
		// Funçao PRINTF para facilitar a formatação ao imprimir na tela //
			$prod = "Leite";
			$preco = 4.5;
			echo "O $prod custa R$ ".number_format($preco,2,",",".");
			echo "</br>";
			printf ("O %s custa R$ %.2f",$prod,$preco);
			$prod2 = "Arroz";
			$preco2 = 2.2;
			echo "</br>";
			echo "O $prod2 custa R$ ".number_format($preco2,2,",",".");
			echo "</br>";
			printf ("O %s custa R$ %.2f",$prod2,$preco2);
			echo "</br>---------------------</br>";
		// Função PRINT-R para imprimir os parametros de um vetor
			$v0[0] = 5; 
			$v0[1] = 5; 
			$v0[2] = 5; 
			$v0[3] = 5; 
			print_r($v0);
			echo "</br>";
			$v[0] = 2;
			$v[1] = "ondéqueutô";
			$v[2] = 6.5;
			var_dump ($v);
			echo "</br>";
			$v2 = array (3,6,9,12,15);
			var_export ($v2);
			echo "</br>---------------------</br>";
		// Função WORDWRAP para facilitar a quebra de linhas
			$txt = "Nessa aula, veremos uma lista de funções para Strings usando PHP. São funções internas que já existem na linguagem.";
			$t = wordwrap($txt, 35, "</br>\n");
			echo $t;
			echo "</br>";	
		// Função STRLEN para contar o numero de caracteres de uma String
			$stot = strlen($txt);
			echo "Total de Caracteres da String : $stot";
			echo "</br>---------------------</br>";
		// Função TRIM para remover espaços sobressalentes nas Strings
			$nome = "   KELWYN LEVRONE ";
			$tnome = strlen($nome);
			echo "A string: $nome</br>";
			echo "Tem $tnome caracteres</br>";
			$novonome = trim($nome);
			$tnovo = strlen($novonome);
			echo "A string: $novonome</br>";
			echo "Tem $tnovo caracteres</br>";
			echo (ltrim($nome));
			echo (strlen(ltrim($nome)));
			echo "</br>";	
			echo (rtrim($nome));
			echo (strlen(rtrim($nome)));
			echo "</br>---------------------</br>";
		// Funçao str_word_count para contar o numero de palavras em uma string
			//$txt = "Nessa aula, veremos uma lista de funções para Strings usando PHP. São funções internas que já existem na linguagem.";
			$txtc = str_word_count($txt);
			echo "A String '$t' </br> Possui $txtc palavras";
			echo "</br>---------------------</br>";
		// Função EXPLODE para eliminar determinados caracteres de uma string e ocasionalmente coloca-los em um vetor
			$frase = "Nessa aula, veremos uma lista de funções para Strings usando PHP";
			$vetor = explode(" ",$frase);
			print_r ($vetor);
			echo "</br>---------------------</br>";
		// Função str_split para dividir a strig em letras 
			//$nome = "   KELWYN LEVRONE ";
			$vetor2 = str_split($novonome);
			print_r($vetor2);
			echo "</br>---------------------</br>";
		// Função IMPLODE para incluir determinados elementos em uma string
			$vetor3 = array("KELWYN","LEVRONE","GOMES");
			$frase2 = implode("/", $vetor3);
			echo $frase2;
			echo "</br>---------------------</br>";
		// Função CHR para mostrar qual letra é referente ao codigo inserido
			$letra = chr(100);
			echo "A letra correspondete ao codigo 100 é: $letra";
			echo "</br>---------------------</br>";
		// Função ORD para mostrar qual codigo é referente a letra inserida
			$letra2 = "F";
			$cod = ord($letra2);
			echo "O codigo correspondete à letra F é: $cod";
		
		?>
	</div>
</body>
</html>