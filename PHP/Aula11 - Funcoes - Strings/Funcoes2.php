<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title> Repetição For PHP-HTML</title>
	<link rel="stylesheet" href="../CSS/_css/estilo.css"/>

</head>
<body>
	<div>	
	<?php
	// Função 'strtolower' para transformar caracteres em letras minusculas
		$nome = "KELWYN LEVRONE";
		$nomeL = strtolower($nome);
		echo $nomeL;
	// Função 'strtoupper' para transformar em letras maiusculas
		$nomeU = strtoupper($nomeL);
		echo "</br>$nomeU";
		echo "</br>-------------------------------</br>";
	// Função 'ucfirst' para transformar o primeiro caractere da string em maiusculo
		$nomeUC = ucfirst($nomeL);
		echo $nomeUC;
		$n1 = "kelwyn";
		$n2 = "levrone";
		$n1U = ucfirst($n1);
		$n2U = ucfirst($n2);
		echo "</br>$n1U $n2U";
		echo "</br>-------------------------------</br>";
	// Função 'ucwords' para transformar o primeiro caractere de cada palavra da string em maiusculo
		$nomeUW = ucwords($nomeL);
		echo $nomeUW;
	// Funçao 'strrev' para reverter a ordem dos caracteres da string
		$nomeR = strrev($nomeUW);
		echo "</br>$nomeR";
		echo "</br>-------------------------------</br>";
	// Função 'strpos' para encontrar uma palavra ou elemento especifico dentro da string
		$frase = "Aula do Curso de PHP criado pelo professor Gustavo Guanabara";
		$pos = strpos($frase,"PHP");
		echo "Na string '$frase' a Posição da palavra 'PHP' é: $pos";
	// Funçao 'stripos' para encontrar uma palavra ou elemento especifico dentro de uma string, ignorando se esta em maiusculo ou minusculo
		$pos2 = stripos($frase,"guanabara");
		echo "</br>Na string '$frase' a Posição da palavra 'Guanabara' é: $pos2";
		echo "</br>-------------------------------</br>";
	// Função 'substr_count' para contar quantas vezes uma palavra ou elemento é encontrado na string
		$cont = substr_count($frase,"a");
		echo "Na string '$frase' a letra 'A' é encontrada: $cont vezes";
	// Função 'substr' para criar sub-strings de acordo com os parametros passados
		echo "</br>-------------------------------</br>";
		print(substr($frase,0,20));
		echo "</br>";
		print(strlen($frase));
		echo "</br>-------------------------------</br>";
	// Função 'str_pad' para criar um espaço determinado na string
		$frase2 = "PHP";
		$nfrase = str_pad($frase2,30,"x",STR_PAD_RIGHT);
		echo "Aula de $nfrase do curso de $nfrase.";
	// Função 'str_repeat' para repetir a string determinadas vezes
		echo "</br>";
		print(str_repeat("-",40));
	// Função 'str_replace' para substituir caracteres na string
		//$frase = "Aula do Curso de PHP criado pelo professor Gustavo Guanabara"
		$frasen = str_replace("Gustavo Guanabara","Kelwyn Levrone",$frase);
		echo "</br>$frasen";
	
	
	?>
	</div>
</body>
</html>